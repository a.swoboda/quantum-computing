#! /usr/bin/env python3
"""Example implementation of Grover's algorithm"""

import math
import cirq


def make_oracle(i_qubits, o_qubits, solutions):
    """Create an "oracle" circuit with the solutions provided"""
    return cirq.Circuit(
        cirq.ControlledGate(cirq.X, len(i_qubits), solution).on(
            *i_qubits, o_qubits) for solution in solutions)


class Grover:
    """Build Grover circuits from an oracle"""

    def __init__(self, oracle, working_qubit):
        """Initialize with an oracle, and tag the working qubit (register)"""
        if not working_qubit in oracle.all_qubits():
            raise ValueError("working qubit not in oracle circuit")
        self.oracle = oracle
        self.working_qubit = working_qubit

    def second_reflection(self):
        """Create the second Grover reflection"""
        input_qubits = self.oracle.all_qubits().difference(
            {self.working_qubit})
        controlled_not = cirq.ControlledGate(cirq.X, len(input_qubits))
        circuit = cirq.Circuit()
        circuit.append([cirq.H(q) for q in input_qubits])
        circuit.append([cirq.X(q) for q in input_qubits])
        circuit.append(controlled_not.on(*input_qubits, self.working_qubit))
        circuit.append([cirq.X(q) for q in input_qubits])
        circuit.append([cirq.H(q) for q in input_qubits])
        return circuit

    def circuit(self, repetitions):
        """Create a circuit implementing Grover's algorithm, with given number of iterations"""
        circuit = cirq.Circuit()
        circuit.append(cirq.X(self.working_qubit))
        circuit.append(cirq.H.on_each(self.oracle.all_qubits()))
        for _ in range(repetitions):
            circuit.append(self.oracle)
            circuit.append(self.second_reflection())
        return circuit


def grover_probability(n_qubits, n_iterations, n_solutions=1):
    """Compute the probability of measuring a correct solution"""
    return math.sin(
        (2 * n_iterations + 1
         ) * math.asin(math.sqrt(n_solutions / 2**n_qubits)))**2 / n_solutions


if __name__ == '__main__':
    LENGTH = 4
    N_SIMULATIONS = 100

    QUBITS = [cirq.GridQubit(0, i) for i in range(LENGTH)]

    # Define the oracle
    REFERENCE = {2}
    DESIRED_BINARY = [
        format(d, 'b')[-LENGTH + 1:].zfill(LENGTH - 1) for d in REFERENCE
    ]
    print("Gesucht:", ", ".join(DESIRED_BINARY))
    DESIRED_BYTES = [bytes(int(i) for i in d) for d in DESIRED_BINARY]
    ORACLE = make_oracle(QUBITS[:-1], QUBITS[-1], DESIRED_BYTES)

    G = Grover(cirq.Circuit(ORACLE), QUBITS[-1])
    for iterations in range(1, 3):
        c = G.circuit(iterations)
        c.append(cirq.measure(*QUBITS[0:-1], key='result'))
        print(c)
        result = cirq.Simulator().run(program=c, repetitions=N_SIMULATIONS)
        frequencies = result.histogram(
            key='result',
            fold_func=lambda bits: ''.join(str(int(b)) for b in bits))
        print("n =", iterations)
        print('\n'.join([
            "P({0}) = {1:.2f}, h({0}) = {2:.2f}".format(
                d, grover_probability(LENGTH - 1, iterations, len(REFERENCE)),
                frequencies[d] / N_SIMULATIONS) for d in DESIRED_BINARY
        ]))
